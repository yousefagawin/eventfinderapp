package com.example.eventfinderapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "page")
data class Page(
//    @PrimaryKey val repoId: Long,
//    val prevKey: Int?,
//    val nextKey: Int?

    @PrimaryKey val number: Int,

    val size: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)