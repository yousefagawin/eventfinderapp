package com.example.eventfinderapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.eventfinderapp.api._Embedded
import com.google.gson.annotations.SerializedName

data class MusicEvent(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("url") val url: String,
    @SerializedName("dates") val dates: Dates?,
    @SerializedName("distance") val distance: Float?,
    @SerializedName("units") val units: String?,
    @SerializedName("images") val images: List<Image> = emptyList()
)

data class Dates(
    @SerializedName("initialStartDate") val initialStartDate: InitialStartDate? = null,
)

data class Image(
    @SerializedName("ratio") val ratio: String?,
    @SerializedName("url") val url: String?,
    @SerializedName("width") val width: Int?,
    @SerializedName("height") val height: Int?,
    @SerializedName("fallback") val fallback: Boolean?,
)

data class InitialStartDate(
    @SerializedName("localDate") val localDate: String,
    @SerializedName("localTime") val localTime: String
)