package com.example.eventfinderapp

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

//will use to store constant values
object Constants {

    const val GLIDE_TIMEOUT = 15000 //15s
    const val LOCATION_PERMISSION_CODE = 2

    const val TICKET_MASTER_DATE_FORMAT = "yyyy-MM-dd"
    const val TICKET_MASTER_TIME_FORMAT = "HH:mm:ss"

    fun Long?.convertMillisToDateString(dateFormat: String, timezone: String?): String? {
        try {
            val sdf = SimpleDateFormat(dateFormat)
            if (!timezone.isNullOrBlank()) {
                sdf.timeZone = TimeZone.getTimeZone(timezone)
            }
            val dateString = sdf.format(this)
            return dateString
        } catch (e: Exception) {
            return ""
        }
    }

    fun getCurrentTimeForTicketMasterAPI(): String {
        val currentTime = Calendar.getInstance().timeInMillis
        val sdfDate = SimpleDateFormat(TICKET_MASTER_DATE_FORMAT)
        val sdfTime = SimpleDateFormat(TICKET_MASTER_TIME_FORMAT)
        return sdfDate.format(currentTime) + "T" + sdfTime.format(currentTime) + "Z"
    }
}