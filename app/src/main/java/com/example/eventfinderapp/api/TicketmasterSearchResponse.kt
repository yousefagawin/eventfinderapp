package com.example.eventfinderapp.api

import com.example.eventfinderapp.model.Page
import com.example.eventfinderapp.model.MusicEvent
import com.google.gson.annotations.SerializedName

data class TicketmasterSearchResponse (
    @SerializedName("_embedded") val _embedded: _Embedded?,
    @SerializedName("page") val page: Page?,
)

data class _Embedded(
    @SerializedName("events") val events: List<MusicEvent> = emptyList(),
    )