package com.example.eventfinderapp.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Query

interface EventsService {

    @GET("discovery/v2/events.json?" +
            "classificationName=music" +  //we only get the music events in the whole app
            "&unit=km" +                    // wee only use KM as our unit on the app for now
            "&apikey=$API_KEY"
    )
    suspend fun getMusicEvents(
        @Query("keyword") keyword: String?,
        @Query("page") page: String,
        @Query("size") size: String,
        @Query("geoPoint") geoPoint: String?,
        @Query("radius") radius: String?,
        @Query("startDateTime") startDateTime: String?
    ): TicketmasterSearchResponse

    companion object {
        private const val BASE_URL = "https://app.ticketmaster.com/"

        private const val API_KEY = "lK5IAqsNtLLdEW8SKl5pdZayQLseAOV7"

        fun create(): EventsService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EventsService::class.java)
        }
    }
}
