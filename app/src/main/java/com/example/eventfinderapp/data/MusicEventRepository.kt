package com.example.eventfinderapp.data

import androidx.paging.*
import com.example.eventfinderapp.api.EventsService


/**
 * Repository class that can use to works with local and remote data sources.
 *
 */


class MusicEventRepository (private val eventsService: EventsService) {

    fun getSearchResults(
        query: String?,
        geoPoint: String?,
        radius: String?,
        startDateTime: String?
    ) =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                MusicEventPagingSource(
                    eventsService,
                    query,
                    geoPoint,
                    radius,
                    startDateTime
                )
            }
        ).flow
}