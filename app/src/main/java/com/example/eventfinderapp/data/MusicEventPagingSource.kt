package com.example.eventfinderapp.data

import androidx.paging.PagingSource
import com.example.eventfinderapp.api.EventsService
import com.example.eventfinderapp.model.MusicEvent
import retrofit2.HttpException
import java.io.IOException

private const val TCIKETMASTER_STARTING_PAGE_INDEX = 0


 class MusicEventPagingSource(
    private val eventsService: EventsService,
    private val query: String?,

    private val geoPoint: String?,
    private val radius: String?,
    private val startDateTime: String?
) : PagingSource<Int, MusicEvent>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MusicEvent> {
        val position = params.key ?: TCIKETMASTER_STARTING_PAGE_INDEX

        return try {
            val response = eventsService.getMusicEvents(
                keyword = query,
                page = position.toString(),
                size = params.loadSize.toString(),
                geoPoint = geoPoint,
                radius = radius,
                startDateTime = startDateTime,
            )

            val photos = response._embedded?.events
            LoadResult.Page(
                data = if(photos.isNullOrEmpty()) emptyList() else photos,
                prevKey = if (position == TCIKETMASTER_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (photos.isNullOrEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

}