package com.example.eventfinderapp.ui

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.eventfinderapp.data.MusicEventRepository
import com.example.eventfinderapp.model.MusicEvent
import kotlinx.coroutines.flow.Flow

class MainViewModel  (
    private val musicEventRepository: MusicEventRepository
    ) : ViewModel() {

//    val currentQuery =  MutableLiveData<String> ("")
//
//    val currentDistance = MutableLiveData<String>("")
//    val currentLoc= MutableLiveData <String>  ("34.047390,-117.901596")

    //no need to make this as livedata to avoid frequent loading of data
    var currentLocString = ""
    val musicEvent = MediatorLiveData<PagingData<MusicEvent>?>()

    private var currentSearchResult: Flow<PagingData<MusicEvent>>? = null

    fun searchRepo(
        queryString: String?,
        distanceString: String?,
        startDateTime: String?
    ): Flow<PagingData<MusicEvent>> {

        val newResult: Flow<PagingData<MusicEvent>> = musicEventRepository.getSearchResults(
           query = queryString,
            geoPoint = currentLocString,
            radius = distanceString,
            startDateTime = startDateTime
        ).cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }


    companion object {
        private const val CURRENT_QUERY = "current_query"
        private const val DEFAULT_QUERY = ""

        private const val CURRENT_DISTANCE = "current_distance"
        private const val DEFAULT_DISTANCE = "10000"

        private const val CURRENT_LOC = "current_loc"
        private const val DEFAULT_LOC = "34.047390,-117.901596"

        private const val TAG = "GalleryViewModel"
    }
}