package com.example.eventfinderapp.ui

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.eventfinderapp.Constants
import com.example.eventfinderapp.R
import com.example.eventfinderapp.model.MusicEvent

class EventsAdapter(val itemClickListener: OnItemClickListener) :
    PagingDataAdapter<MusicEvent, RecyclerView.ViewHolder, >(UIMODEL_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RepoViewHolder.create(parent)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.event_item
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewItem = getItem(position)
        viewItem.let {

            (holder as RepoViewHolder).bind(viewItem)
        }
    }

    companion object {
        private val UIMODEL_COMPARATOR = object : DiffUtil.ItemCallback<MusicEvent>() {
            override fun areItemsTheSame(oldItem: MusicEvent, newItem: MusicEvent): Boolean {
                return (oldItem.id == newItem.id)
            }

            override fun areContentsTheSame(oldItem: MusicEvent, newItem: MusicEvent): Boolean =
                oldItem == newItem
        }
    }

    interface OnItemClickListener {
        fun onItemClick(event: MusicEvent)
    }
}

class RepoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val event_name: TextView = view.findViewById(R.id.event_name)
    private val event_startdate: TextView = view.findViewById(R.id.event_startdate)
    private val event_distance: TextView = view.findViewById(R.id.event_distance)
    private val event_image: ImageView = view.findViewById(R.id.event_image)

    private var repo: MusicEvent? = null

    init {
        view.setOnClickListener {
            repo?.url?.let { url ->
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                view.context.startActivity(intent)
            }
        }
    }

    fun bind(repo: MusicEvent?) {
        if (repo == null) {
            val resources = itemView.resources
            event_name.text = resources.getString(R.string.loading)
            event_startdate.visibility = View.GONE
            event_distance.visibility = View.GONE
//            event_image.setImageResource(R.drawable.ic_music_black)
        } else {
            showRepoData(repo)
        }
    }

    private fun showRepoData(musicEvent: MusicEvent) {
        this.repo = musicEvent
        event_name.text = musicEvent.name

        event_startdate.text = musicEvent.dates?.initialStartDate?.localDate

        if(musicEvent.distance != null || musicEvent.units != null){
            event_distance.text = "${musicEvent.distance} ${musicEvent.units}"
        } else {
            event_distance.text = "(Enable GPS and Input Distance value)"
        }

        musicEvent.images[0].let {
            Glide.with(itemView.context)
                .load(it.url)
                .placeholder(R.color.white)
                .error(R.drawable.ic_music_black)
                .timeout(Constants.GLIDE_TIMEOUT)
                .apply(RequestOptions.circleCropTransform().dontAnimate())
                .into(event_image)
        }
    }

    companion object {
        fun create(parent: ViewGroup): RepoViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.event_item, parent, false)
            return RepoViewHolder(view)
        }
    }
}
